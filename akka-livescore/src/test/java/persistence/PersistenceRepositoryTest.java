package persistence;

import entities.Commentary;
import entities.SoccerMatch;
import entities.Statistics;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;

import static entities.enums.StatisticsType.GOAL;

public class PersistenceRepositoryTest {
    private PersistenceRepository repository;

    @Before
    public void setUp() {
        repository = new PersistenceRepository();
    }

    @After
    public void tearDown() {
        repository.close();
    }

    @Test
    public void repositoryShouldStoreSoccerMatchAndStatisticsEntities() {
        SoccerMatch soccerMatch = new SoccerMatch(1);
        Statistics stats = new Statistics();
        stats.setSoccerMatch(soccerMatch);
        stats.setTime(LocalDateTime.now());
        stats.setOwner("Ibragimovic");
        stats.setType(GOAL);

        repository.persist(stats);

        Assert.assertEquals("One soccerMatch record should be stored", 1, repository.findAllSoccerMatches().size());
        Assert.assertEquals("One statistics record should be stored", 1, repository.findStatistics(soccerMatch.getMatchId()).size());
        Assert.assertEquals("Incorrect 'owner' field value", "Ibragimovic", repository.findStatistics(soccerMatch.getMatchId()).get(0).getOwner());
        Assert.assertEquals("No commentary record should be stored", 0, repository.findCommentaries(soccerMatch.getMatchId()).size());

    }

    @Test
    public void repositoryShouldStoreSoccerMatchAndCommentaryEntities() {
        SoccerMatch soccerMatch = new SoccerMatch(1);
        Commentary commentary = new Commentary();
        commentary.setSoccerMatch(soccerMatch);
        commentary.setTime(LocalDateTime.now());
        commentary.setText("GOAL!");
        repository.persist(commentary);

        Assert.assertEquals("One soccerMatch record should be stored", 1, repository.findAllSoccerMatches().size());
        Assert.assertEquals("One commentary record should be stored", 1, repository.findCommentaries(soccerMatch.getMatchId()).size());
        Assert.assertEquals("One commentary record should be stored", "GOAL!", repository.findCommentaries(soccerMatch.getMatchId()).get(0).getText());
        Assert.assertEquals("No statistics record should be stored", 0, repository.findStatistics(soccerMatch.getMatchId()).size());

    }

}
