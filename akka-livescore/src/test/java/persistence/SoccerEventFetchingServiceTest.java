package persistence;

import entities.Commentary;
import entities.SoccerMatch;
import entities.Statistics;
import models.SoccerMatchEvent;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.List;

import static entities.enums.StatisticsType.GOAL;

public class SoccerEventFetchingServiceTest {
    private PersistenceRepository repository;
    private SoccerEventFetchingService soccerEventFetchingService;

    @Before
    public void setUp() {
        repository = new PersistenceRepository();
        soccerEventFetchingService = new SoccerEventFetchingService(repository);
    }

    @After
    public void tearDown() {
        repository.close();
    }

    @Test
    public void shouldFetchSoccerMatchEventByMatchId() {
        SoccerMatch soccerMatch = new SoccerMatch(1);
        repository.persist(getDummyCommentary(soccerMatch));
        repository.persist(getDummyStatistics(soccerMatch));

        SoccerMatchEvent soccerMatchEvents = soccerEventFetchingService.fetchSoccerMatchEvent(soccerMatch.getMatchId());
        Assert.assertEquals(soccerMatch.getMatchId().intValue(), soccerMatchEvents.getMatchId());

        Assert.assertEquals(1, soccerMatchEvents.getCommentaries().size());
        Assert.assertEquals("GOAL!", soccerMatchEvents.getCommentaries().get(0).getText());
        Assert.assertNotNull(soccerMatchEvents.getCommentaries().get(0).getTime());

        Assert.assertEquals(1, soccerMatchEvents.getStatistics().size());
        Assert.assertEquals("Ibragimovic", soccerMatchEvents.getStatistics().get(0).getOwner());
        Assert.assertEquals(GOAL, soccerMatchEvents.getStatistics().get(0).getType());
        Assert.assertNotNull(soccerMatchEvents.getStatistics().get(0).getTime());
    }

    @Test
    public void shouldFetchAllSoccerMatchEvents() {
        SoccerMatch soccerMatch = new SoccerMatch(1);
        repository.persist(getDummyCommentary(soccerMatch));
        repository.persist(getDummyCommentary(soccerMatch));
        repository.persist(getDummyStatistics(soccerMatch));
        repository.persist(getDummyStatistics(soccerMatch));

        List<SoccerMatchEvent> soccerMatchEvents = soccerEventFetchingService.fetchAllSoccerMatchEvents();
        Assert.assertEquals("One soccerMatchEvent is expected to be fetched", 1, soccerMatchEvents.size());
        Assert.assertEquals("Two commentaries per soccerMatch are expected", 2, soccerMatchEvents.get(0).getCommentaries().size());
        Assert.assertEquals("Two statistics per soccerMatch are expected", 2, soccerMatchEvents.get(0).getStatistics().size());



        repository.persist(getDummyCommentary(new SoccerMatch(2)));
        repository.persist(getDummyCommentary(new SoccerMatch(3)));
        repository.persist(getDummyCommentary(new SoccerMatch(4)));

        soccerMatchEvents = soccerEventFetchingService.fetchAllSoccerMatchEvents();
        Assert.assertEquals("4 soccerMatchEvents are expected to be fetched", 4, soccerMatchEvents.size());

    }


    private Commentary getDummyCommentary(SoccerMatch soccerMatch) {
        Commentary commentary = new Commentary();
        commentary.setSoccerMatch(soccerMatch);
        commentary.setTime(LocalDateTime.now());
        commentary.setText("GOAL!");

        return commentary;
    }

    private Statistics getDummyStatistics(SoccerMatch soccerMatch) {
        Statistics stats = new Statistics();
        stats.setSoccerMatch(soccerMatch);
        stats.setTime(LocalDateTime.now());
        stats.setOwner("Ibragimovic");
        stats.setType(GOAL);
        return stats;
    }
}
