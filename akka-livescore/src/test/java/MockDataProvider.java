import entities.enums.StatisticsType;
import models.CommentaryEvent;
import models.StatisticsEvent;
import utils.SoccerEventProvider;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;

import static java.time.LocalDateTime.of;

public class MockDataProvider implements SoccerEventProvider {
    public static final Integer MATCH1_ID = 1;
    public static final Integer MATCH2_ID = 2;

    public static final StatisticsEvent STATISTICS_1 = new StatisticsEvent(StatisticsType.GOAL, of(LocalDate.now(), LocalTime.of(10, 59)), "Ibragimovic");
    public static final StatisticsEvent STATISTICS_2 = new StatisticsEvent(StatisticsType.YELLOW_CARD, of(LocalDate.now(), LocalTime.of(11, 59)), "Messi");

    public static final CommentaryEvent COMMENT1 = new CommentaryEvent(LocalDateTime.now(), "Dummy comment ONE");
    public static final CommentaryEvent COMMENT2 = new CommentaryEvent(LocalDateTime.now(), "Dummy comment TWO");

    private final Map<Integer, StatisticsEvent> statisticsMap = new HashMap<>();
    private final Map<Integer, CommentaryEvent> commentaryMap = new HashMap<>();

    public MockDataProvider() {
        statisticsMap.put(MATCH1_ID, STATISTICS_1);
        statisticsMap.put(MATCH2_ID, STATISTICS_2);

        commentaryMap.put(MATCH1_ID, COMMENT1);
        commentaryMap.put(MATCH2_ID, COMMENT2);
    }

    @Override
    public StatisticsEvent getStatistics(int matchId) {
        return statisticsMap.getOrDefault(matchId, new StatisticsEvent(StatisticsType.NOTHING, null, ""));
    }

    @Override
    public CommentaryEvent getCommentary(int matchId) {
        return commentaryMap.getOrDefault(matchId, new CommentaryEvent(null, ""));
    }

}
