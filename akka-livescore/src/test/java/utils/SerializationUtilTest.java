package utils;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDateTime;

import static java.time.format.DateTimeFormatter.ofPattern;
import static org.apache.commons.lang3.StringUtils.strip;
import static org.apache.commons.lang3.StringUtils.stripEnd;

public class SerializationUtilTest {

    @Test
    public void verifyLocalDateTimeSerialization() {
        LocalDateTime time = LocalDateTime.now();

        String expectedFormattedTime = ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSSS").format(time);
        expectedFormattedTime = stripEnd(expectedFormattedTime, "0"); //ObjectMapper removes trailing 0 during serialization

        String actualFormattedTime = SerializationUtil.serialize(time);
        actualFormattedTime = strip(actualFormattedTime, "\"");

        Assert.assertEquals(expectedFormattedTime, actualFormattedTime);
    }

}
