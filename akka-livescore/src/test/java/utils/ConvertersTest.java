package utils;

import entities.Commentary;
import entities.Statistics;
import models.CommentaryEvent;
import models.StatisticsEvent;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDateTime;

import static entities.enums.StatisticsType.GOAL;
import static utils.converters.CommentaryEntityToEventConverter.COMMENTARY_ENTITY_TO_EVENT_CONVERTER;
import static utils.converters.CommentaryEventToEntityConverter.COMMENTARY_EVENT_TO_ENTITY_CONVERTER;
import static utils.converters.StatisticsEntityToEventConverter.STATISTICS_ENTITY_TO_EVENT_CONVERTER;
import static utils.converters.StatisticsEventToEntityConverter.STATISTICS_EVENT_TO_ENTITY_CONVERTER;

public class ConvertersTest {

    @Test
    public void convertCommentaryFromEntityToEvent() {
        Commentary expected = getDummyCommentaryEntity();
        CommentaryEvent actual = COMMENTARY_ENTITY_TO_EVENT_CONVERTER.convert(expected);
        Assert.assertEquals(expected.getText(), actual.getText());
        Assert.assertEquals(expected.getTime(), actual.getTime());
    }

    @Test
    public void convertCommentaryFromEventToEntity() {
        CommentaryEvent expected = getDummyCommentaryEvent();
        Commentary actual = COMMENTARY_EVENT_TO_ENTITY_CONVERTER.convert(expected);
        Assert.assertEquals(expected.getText(), actual.getText());
        Assert.assertEquals(expected.getTime(), actual.getTime());
    }

    @Test
    public void convertStatisticsFromEntityToEvent() {
        Statistics expected = getDummyStatisticsEntity();
        StatisticsEvent actual = STATISTICS_ENTITY_TO_EVENT_CONVERTER.convert(expected);
        Assert.assertEquals(expected.getTime(), actual.getTime());
        Assert.assertEquals(expected.getOwner(), actual.getOwner());
        Assert.assertEquals(expected.getType(), actual.getType());
    }

    @Test
    public void convertStatisticsFromEventToEntity() {
        StatisticsEvent expected = getDummyStatisticsEvent();
        Statistics actual = STATISTICS_EVENT_TO_ENTITY_CONVERTER.convert(expected);
        Assert.assertEquals(expected.getTime(), actual.getTime());
        Assert.assertEquals(expected.getOwner(), actual.getOwner());
        Assert.assertEquals(expected.getType(), actual.getType());
    }

    private Commentary getDummyCommentaryEntity() {
        Commentary commentary = new Commentary();
        commentary.setTime(LocalDateTime.now());
        commentary.setText("GOAL!");

        return commentary;
    }

    private Statistics getDummyStatisticsEntity() {
        Statistics stats = new Statistics();
        stats.setTime(LocalDateTime.now());
        stats.setOwner("Ibragimovic");
        stats.setType(GOAL);
        return stats;
    }


    private CommentaryEvent getDummyCommentaryEvent() {
        return new CommentaryEvent(LocalDateTime.now(), "comment");
    }

    private StatisticsEvent getDummyStatisticsEvent() {
        return new StatisticsEvent(GOAL, LocalDateTime.now(), "ibragimovic");
    }


}
