import actors.DbPersistenceActor;
import actors.MatchActor;
import actors.PullerActor;
import actors.messages.*;
import akka.actor.Actor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.testkit.TestActorRef;
import akka.testkit.TestKit;
import models.CommentaryEvent;
import models.StatisticsEvent;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import persistence.PersistenceRepository;
import scala.concurrent.duration.FiniteDuration;

import java.util.concurrent.TimeUnit;

import static java.util.Optional.empty;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class TestAkka {
    private static ActorSystem system;

    @BeforeClass
    public static void setup() {
        system = ActorSystem.create();
    }

    @AfterClass
    public static void tearDown() {
        try {
            TestKit.shutdownActorSystem(system, FiniteDuration.create(5, TimeUnit.SECONDS), true);
        } catch (Exception e) {
            e.printStackTrace();
            fail("Failed to de-initialize test");
        }
    }

    @Test
    public void itShouldGetStatisticResponse() {
        final TestKit probe = new TestKit(system);
        final TestActorRef<Actor> actorTestActorRef = TestActorRef.create(system, PullerActor.props(new MockDataProvider()));

        actorTestActorRef.tell(new StatisticsMessageRequest("abc", MockDataProvider.MATCH1_ID), probe.testActor());
        StatisticsMessageResponse response1 = probe.expectMsgClass(StatisticsMessageResponse.class);

        verifyStatistics(response1, "abc", MockDataProvider.MATCH1_ID, MockDataProvider.STATISTICS_1);

        actorTestActorRef.tell(new StatisticsMessageRequest("xyz", MockDataProvider.MATCH2_ID), probe.testActor());
        StatisticsMessageResponse response2 = probe.expectMsgClass(StatisticsMessageResponse.class);

        verifyStatistics(response2, "xyz", MockDataProvider.MATCH2_ID, MockDataProvider.STATISTICS_2);

    }

    private void verifyStatistics(StatisticsMessageResponse actualResponse, String expectedReqId, Integer expectedMatchId, StatisticsEvent expectedStatisticsEvent) {
        Assert.assertEquals("requestId of request and response messages should match", actualResponse.getRequestId(), expectedReqId);
        Assert.assertEquals("matchId of request and response messages should match", actualResponse.getMatchId(), expectedMatchId.intValue());
        Assert.assertEquals("received statistics are incorrect", actualResponse.getStatisticsEvent(), expectedStatisticsEvent);
    }

    @Test
    public void itShouldGetCommentaryResponse() {
        final TestKit probe = new TestKit(system);
        final TestActorRef<Actor> actorTestActorRef = TestActorRef.create(system, PullerActor.props(new MockDataProvider()));

        actorTestActorRef.tell(new CommentaryMessageRequest("abc", MockDataProvider.MATCH1_ID), probe.testActor());
        CommentaryMessageResponse response1 = probe.expectMsgClass(CommentaryMessageResponse.class);

        verifyCommentary(response1, "abc", MockDataProvider.MATCH1_ID, MockDataProvider.COMMENT1);

        actorTestActorRef.tell(new CommentaryMessageRequest("xyz", MockDataProvider.MATCH2_ID), probe.testActor());
        CommentaryMessageResponse response2 = probe.expectMsgClass(CommentaryMessageResponse.class);

        verifyCommentary(response2, "xyz", MockDataProvider.MATCH2_ID, MockDataProvider.COMMENT2);

    }

    private void verifyCommentary(CommentaryMessageResponse actualResponse, String expectedReqId, Integer expectedMatchId, CommentaryEvent expectedCommentEvent) {
        Assert.assertEquals("requestId of request and response messages should match", actualResponse.getRequestId(), expectedReqId);
        Assert.assertEquals("matchId of request and response messages should match", actualResponse.getMatchId(), expectedMatchId.intValue());
        Assert.assertEquals("received commentary are incorrect", actualResponse.getCommentaryEvent(), expectedCommentEvent);
    }

    @Test
    public void matchActorShouldPassEventMessagesToDbActor() {
        final TestKit probe = new TestKit(system);

        TestActorRef<Actor> pullerActor = TestActorRef.create(system, PullerActor.props(new MockDataProvider()));
        ActorRef dbActor = probe.testActor(); // matchActor will be sending msgs to dbActor
        TestActorRef<Actor> matchActor = TestActorRef.create(system, MatchActor.props(pullerActor, dbActor));

        SoccerEventRequest eventRequest = new SoccerEventRequest(MockDataProvider.MATCH1_ID);
        matchActor.tell(eventRequest, ActorRef.noSender());
        CommentaryMessageResponse actualCommentResponse = probe.expectMsgClass(CommentaryMessageResponse.class);
        StatisticsMessageResponse actualStatResponse = probe.expectMsgClass(StatisticsMessageResponse.class);

        verifyCommentary(actualCommentResponse, eventRequest.getId(), eventRequest.getMatchId(), MockDataProvider.COMMENT1);
        verifyStatistics(actualStatResponse, eventRequest.getId(), eventRequest.getMatchId(), MockDataProvider.STATISTICS_1);

    }

    @Test
    public void dbPersistenceActorShouldCallPersistForStatistics() {
        final TestKit probe = new TestKit(system);
        PersistenceRepository mockPersistenceRepository = mock(PersistenceRepository.class);
        when(mockPersistenceRepository.findSoccerMatch(anyInt())).thenReturn(empty());

        TestActorRef<Actor> dbActor = TestActorRef.create(system, DbPersistenceActor.props(mockPersistenceRepository));
        dbActor.tell(new StatisticsMessageResponse("abc", MockDataProvider.MATCH1_ID, MockDataProvider.STATISTICS_1), probe.testActor());
        probe.expectNoMessage();

        Mockito.verify(mockPersistenceRepository, times(1)).findSoccerMatch(MockDataProvider.MATCH1_ID);
        Mockito.verify(mockPersistenceRepository, times(1)).persist(any());
    }

    @Test
    public void dbPersistenceActorShouldCallPersistForCommentary() {
        final TestKit probe = new TestKit(system);
        PersistenceRepository mockPersistenceRepository = mock(PersistenceRepository.class);
        when(mockPersistenceRepository.findSoccerMatch(anyInt())).thenReturn(empty());

        TestActorRef<Actor> dbActor = TestActorRef.create(system, DbPersistenceActor.props(mockPersistenceRepository));
        dbActor.tell(new CommentaryMessageResponse("abc", MockDataProvider.MATCH1_ID, MockDataProvider.COMMENT1), probe.testActor());
        probe.expectNoMessage();

        Mockito.verify(mockPersistenceRepository, times(1)).findSoccerMatch(MockDataProvider.MATCH1_ID);
        Mockito.verify(mockPersistenceRepository, times(1)).persist(any());

    }

}
