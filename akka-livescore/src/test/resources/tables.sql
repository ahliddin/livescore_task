-- DDL scripts to create entity tables in case of migrating from H2 to PostgreSQL--

create table SOCCER_MATCH (
    MATCH_ID integer primary key
);

create table STATISTICS (
    ID varchar primary key,
    MATCH_ID integer references SOCCER_MATCH(MATCH_ID) not null,
    TYPE varchar (20) not null,
    TIME TIMESTAMP not null,
    OWNER varchar(100) not null
);

create table COMMENTARY (
    ID varchar primary key,
    MATCH_ID integer references SOCCER_MATCH(MATCH_ID) not null,
    TEXT varchar (255) not null,
    TIME timestamp not null
);


create index IX_STATS_MATCH_ID on statistics (match_id);
create index IX_COMMENTS_MATCH_ID on commentary (match_id);
