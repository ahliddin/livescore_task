package actors.messages;

public class StatisticsMessageRequest {
    private final String id;
    private final int matchId;

    public StatisticsMessageRequest(String id, int matchId) {
        this.id = id;
        this.matchId = matchId;
    }

    public String getId() {
        return id;
    }

    public int getMatchId() {
        return matchId;
    }
}
