package actors.messages;

import java.util.UUID;

public class SoccerEventRequest {
    private final String id = UUID.randomUUID().toString();
    private final int matchId;

    public SoccerEventRequest(int matchId) {
        this.matchId = matchId;
    }

    public String getId() {
        return id;
    }

    public int getMatchId() {
        return matchId;
    }
}
