package actors.messages;

public class CommentaryMessageRequest {
    private final String id;
    private final int matchId;

    public CommentaryMessageRequest(String id, int matchId) {
        this.id = id;
        this.matchId = matchId;
    }

    public String getId() {
        return id;
    }

    public int getMatchId() {
        return matchId;
    }
}

