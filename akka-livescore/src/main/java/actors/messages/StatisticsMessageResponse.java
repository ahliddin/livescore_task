package actors.messages;

import models.StatisticsEvent;

public class StatisticsMessageResponse {
    private final String requestId;
    private final StatisticsEvent statisticsEvent;
    private final int matchId;

    public StatisticsMessageResponse(String requestId, int matchId, StatisticsEvent statisticsEvent) {
        this.requestId = requestId;
        this.matchId = matchId;
        this.statisticsEvent = statisticsEvent;
    }

    public String getRequestId() {
        return requestId;
    }

    public int getMatchId() {
        return matchId;
    }

    public StatisticsEvent getStatisticsEvent() {
        return statisticsEvent;
    }
}
