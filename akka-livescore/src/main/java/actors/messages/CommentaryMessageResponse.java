package actors.messages;

import models.CommentaryEvent;

public class CommentaryMessageResponse {
    private final String requestId;
    private final int matchId;
    private final CommentaryEvent commentaryEvent;

    public CommentaryMessageResponse(String requestId, int matchId, CommentaryEvent commentaryEvent) {
        this.requestId = requestId;
        this.matchId = matchId;
        this.commentaryEvent = commentaryEvent;
    }

    public String getRequestId() {
        return requestId;
    }

    public int getMatchId() {
        return matchId;
    }

    public CommentaryEvent getCommentaryEvent() {
        return commentaryEvent;
    }
}
