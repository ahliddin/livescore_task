package actors;

import actors.messages.*;
import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import com.programmer9.validation.Actor;
import models.CommentaryEvent;
import models.StatisticsEvent;

/**
 * Pulls statistics and commentary data from {@link PullerActor} and sends them to {@link DbPersistenceActor}
 */
@Actor
public class MatchActor extends AbstractLoggingActor {

    private final ActorRef pullerActor;


    private final ActorRef dbActor;

    public MatchActor(ActorRef pullerActor, ActorRef dbActor) {
        this.pullerActor = pullerActor;
        this.dbActor = dbActor;
    }

    public static Props props(ActorRef pullerActor, ActorRef dbActor) {
        return Props.create(MatchActor.class, () -> new MatchActor(pullerActor, dbActor));
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(SoccerEventRequest.class, this::onSoccerEventRequest)
                .match(CommentaryMessageResponse.class, this::onCommentary)
                .match(StatisticsMessageResponse.class, this::onStatistics)
                .build();
    }

    private void onStatistics(StatisticsMessageResponse statsResponse) {
        StatisticsEvent event = statsResponse.getStatisticsEvent();
        log().info("Statistics received from pullerActor {}", event);
        dbActor.tell(statsResponse, ActorRef.noSender());
    }

    private void onCommentary(CommentaryMessageResponse commentResponse) {
        CommentaryEvent event = commentResponse.getCommentaryEvent();
        log().info("Commentary received from pullerActor {}", event);
        dbActor.tell(commentResponse, ActorRef.noSender());
    }

    private void onSoccerEventRequest(SoccerEventRequest eventRequest) {
        pullerActor.tell(new CommentaryMessageRequest(eventRequest.getId(), eventRequest.getMatchId()), self());
        pullerActor.tell(new StatisticsMessageRequest(eventRequest.getId(), eventRequest.getMatchId()), self());
    }

}

