package actors;

import actors.messages.CommentaryMessageRequest;
import actors.messages.CommentaryMessageResponse;
import actors.messages.StatisticsMessageRequest;
import actors.messages.StatisticsMessageResponse;
import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import com.programmer9.validation.Actor;
import utils.RandomSoccerEventProvider;
import utils.SoccerEventProvider;

/**
 * Provides randomly generated data from {@link RandomSoccerEventProvider} by default if not valid SoccerEventProvider is passed
 */
@Actor
public class PullerActor extends AbstractLoggingActor {
    private final SoccerEventProvider eventDataProvider;

    public PullerActor(SoccerEventProvider eventDataProvider) {
        this.eventDataProvider = eventDataProvider;
    }

    public static Props props(SoccerEventProvider eventProvider) {
        return Props.create(PullerActor.class, () -> new PullerActor(eventProvider));
    }

    public static Props props() {
        return Props.create(PullerActor.class, () -> new PullerActor(new RandomSoccerEventProvider()));
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(StatisticsMessageRequest.class, this::onStatsRequest)
                .match(CommentaryMessageRequest.class, this::onCommentRequest)
                .build();
    }

    private void onCommentRequest(CommentaryMessageRequest commentRequest) {
        log().info("Commentary request received");
        sender().tell(new CommentaryMessageResponse(commentRequest.getId(), commentRequest.getMatchId(), eventDataProvider.getCommentary(commentRequest.getMatchId())),
                ActorRef.noSender());

    }

    private void onStatsRequest(StatisticsMessageRequest statsRequest) {
        log().info("Statistics request received");
        sender().tell(new StatisticsMessageResponse(statsRequest.getId(), statsRequest.getMatchId(), eventDataProvider.getStatistics(statsRequest.getMatchId())),
                ActorRef.noSender());
    }

}
