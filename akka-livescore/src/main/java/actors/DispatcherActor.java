package actors;

import actors.messages.DispatcherStartMessage;
import actors.messages.SoccerEventRequest;
import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.programmer9.validation.Actor;

import java.time.Duration;

/**
 * DispatcherActor waiting for a {@link DispatcherStartMessage} to create child actors and kick off the scheduler.
 * The scheduler sends {@link SoccerEventRequest} every 10 seconds to child {@link MatchActor}.
 */
@Actor
public class DispatcherActor extends AbstractLoggingActor {
    private static final int[] MATCHES_TO_FOLLOW = {1, 2};

    private final ActorRef dbActor;

    public DispatcherActor(ActorRef dbActor) {
        this.dbActor = dbActor;
    }

    public static Props props(ActorRef dbActor) {
        return Props.create(DispatcherActor.class, () -> new DispatcherActor(dbActor));
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(DispatcherStartMessage.class, this::onStart)
                .build();
    }

    private void onStart(DispatcherStartMessage message) {
        ActorSystem system = getContext().getSystem();

        for (int matchId : MATCHES_TO_FOLLOW) {
            ActorRef pullerActor = system.actorOf(PullerActor.props(), "pullerActor_" + matchId);
            ActorRef matchActor = system.actorOf(MatchActor.props(pullerActor, dbActor), "matchActor_" + matchId);

            log().info("Child PullerActor created: " + pullerActor.toString());
            log().info("Child MatchActor created: " + matchActor.toString());

            system.scheduler()  // scheduling events pull every 10 seconds, so that endpoint users see dynamic data
                    .schedule(Duration.ofMillis(0),
                            Duration.ofSeconds(10),
                            matchActor,
                            new SoccerEventRequest(matchId),
                            system.dispatcher(),
                            ActorRef.noSender());
        }

    }

}
