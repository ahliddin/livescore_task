package actors;

import actors.messages.CommentaryMessageResponse;
import actors.messages.StatisticsMessageResponse;
import akka.actor.AbstractLoggingActor;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;
import com.programmer9.validation.Actor;
import entities.Commentary;
import entities.SoccerMatch;
import entities.Statistics;
import models.CommentaryEvent;
import models.StatisticsEvent;
import persistence.PersistenceRepository;

import static utils.converters.CommentaryEventToEntityConverter.COMMENTARY_EVENT_TO_ENTITY_CONVERTER;
import static utils.converters.StatisticsEventToEntityConverter.STATISTICS_EVENT_TO_ENTITY_CONVERTER;

/**
 * Receives statistics and commentary events, converts them to entity classes and persists them.
 * {@link DbPersistenceActor} is created on separate dispatcher for blocking IO operations, to prevent potential blocking of other actors.
 */

@Actor
public class DbPersistenceActor extends AbstractLoggingActor {
    private PersistenceRepository persistenceRepository;

    public DbPersistenceActor(PersistenceRepository persistenceRepository) {
        this.persistenceRepository = persistenceRepository;
    }

    public static Props props(PersistenceRepository persistenceRepository) {
        return Props.create(DbPersistenceActor.class, () -> new DbPersistenceActor(persistenceRepository))
                .withDispatcher("livescore.blocking-io-dispatcher");
    }

    @Override
    public Receive createReceive() {
        return ReceiveBuilder.create()
                .match(StatisticsMessageResponse.class, this::persistStats)
                .match(CommentaryMessageResponse.class, this::persistCommentary)
                .build();
    }

    private void persistStats(StatisticsMessageResponse statsMsg) {
        StatisticsEvent event = statsMsg.getStatisticsEvent();
        SoccerMatch soccerMatch = persistenceRepository
                .findSoccerMatch(statsMsg.getMatchId())
                .orElse(new SoccerMatch(statsMsg.getMatchId()));

        Statistics entity = STATISTICS_EVENT_TO_ENTITY_CONVERTER.convert(event);
        entity.setSoccerMatch(soccerMatch);
        persistenceRepository.persist(entity);
    }

    private void persistCommentary(CommentaryMessageResponse commentaryMsg) {
        CommentaryEvent event = commentaryMsg.getCommentaryEvent();
        Commentary entity = COMMENTARY_EVENT_TO_ENTITY_CONVERTER.convert(event);

        SoccerMatch soccerMatch = persistenceRepository
                .findSoccerMatch(commentaryMsg.getMatchId())
                .orElse(new SoccerMatch(commentaryMsg.getMatchId()));

        entity.setSoccerMatch(soccerMatch);
        persistenceRepository.persist(entity);
    }

}
