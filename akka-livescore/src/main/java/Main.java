import actors.DbPersistenceActor;
import actors.DispatcherActor;
import actors.messages.DispatcherStartMessage;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import persistence.PersistenceRepository;
import persistence.SoccerEventFetchingService;
import vertx.VertxService;

import java.util.List;

import static akka.actor.ActorRef.noSender;
import static java.util.Arrays.asList;

public class Main {

    public static void main(String[] args) {
        final PersistenceRepository persistenceRepository = new PersistenceRepository();
        final SoccerEventFetchingService soccerEventFetchingService = new SoccerEventFetchingService(persistenceRepository);
        final VertxService vertxService = new VertxService(soccerEventFetchingService);
        final ActorSystem system = ActorSystem.create("akka-livescore-system");

        Thread runtimeHookThread = new Thread(() -> shutdownHook(asList(
                vertxService::stop,
                persistenceRepository::close,
                system::terminate
        )), "RuntimeHookThread");

        Runtime.getRuntime().addShutdownHook(runtimeHookThread);

        vertxService.start();

        ActorRef dbActor = system.actorOf(DbPersistenceActor.props(persistenceRepository), "dbActor"); // should be in separate dispatcher

        ActorRef dispatcherActor = system.actorOf(DispatcherActor.props(dbActor), "dispatcherActor");

        dispatcherActor.tell(new DispatcherStartMessage(), noSender());
    }

    private static void shutdownHook(List<Runnable> servicesToClose) {
        try {
            servicesToClose.forEach(Runnable::run);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
