package vertx;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.ext.web.Router;
import persistence.SoccerEventFetchingService;

import java.util.logging.Logger;

import static java.lang.Integer.parseInt;
import static utils.SerializationUtil.serialize;

/**
 * Exposes REST API to provide JSON serialized  {@link models.SoccerMatchEvent} data
 * The HTTP servers run on different ports, but access the same DB, hence provide the same data
 */
public class VertxService {
    private static Logger LOGGER = Logger.getLogger(VertxService.class.getName());
    private final SoccerEventFetchingService soccerEventFetchingService;
    private final Vertx vertx;

    public VertxService(SoccerEventFetchingService soccerEventFetchingService) {
        this.soccerEventFetchingService = soccerEventFetchingService;
        this.vertx = Vertx.vertx();
    }

    public void start() {
        vertx.deployVerticle(new EndpointVerticle(soccerEventFetchingService, 9999));
        vertx.deployVerticle(new EndpointVerticle(soccerEventFetchingService, 8080));

        LOGGER.info("VertxService is running");
    }

    public void stop() {
        LOGGER.info("VertxService is stopping");
        vertx.close();
    }

    private static class EndpointVerticle extends AbstractVerticle {
        private final SoccerEventFetchingService soccerEventFetchingService;
        private final int port;

        public EndpointVerticle(SoccerEventFetchingService soccerEventFetchingService, int port) {
            this.soccerEventFetchingService = soccerEventFetchingService;
            this.port = port;
        }

        @Override
        public void start() {
            Router router = Router.router(getVertx());
            router.get("/matches")
                    .handler(routingContext -> {
                        routingContext.response()
                                .putHeader("content-type", "application/json")
                                .end(serialize(soccerEventFetchingService.fetchAllSoccerMatchEvents()));
                    });

            router.get("/match/:matchId")
                    .handler(routingContext -> {
                        String matchIdStr = routingContext.request().getParam("matchId");
                        routingContext.response()
                                .putHeader("content-type", "application/json")
                                .end(serialize(soccerEventFetchingService.fetchSoccerMatchEvent(parseInt(matchIdStr))));
                    });

            getVertx().createHttpServer().requestHandler(router).listen(port);
            LOGGER.info("Endpoint Verticle listening on port " + port);
        }
    }
}