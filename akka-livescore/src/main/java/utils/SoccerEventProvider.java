package utils;

import models.CommentaryEvent;
import models.StatisticsEvent;

public interface SoccerEventProvider {

    StatisticsEvent getStatistics(int matchId);

    CommentaryEvent getCommentary(int matchId);

}
