package utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public class SerializationUtil {
    private static final ObjectMapper MAPPER = new CustomizedObjectMapper();

    public static String serialize(Object object) {
        try {
            ObjectWriter objectWriter = MAPPER.writerFor(object.getClass());
            return objectWriter.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new IllegalStateException("Failed to serialize object: " + object, e);
        }
    }

    private static class CustomizedObjectMapper extends ObjectMapper {
        private CustomizedObjectMapper() {
            disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            registerModule(new JavaTimeModule());
        }
    }
}
