package utils.converters;

import entities.Commentary;
import models.CommentaryEvent;

public enum CommentaryEntityToEventConverter implements Converter<Commentary, CommentaryEvent> {

    COMMENTARY_ENTITY_TO_EVENT_CONVERTER;

    @Override
    public CommentaryEvent convert(Commentary entity) {
        return new CommentaryEvent(entity.getTime(), entity.getText());
    }
}
