package utils.converters;

import entities.Statistics;
import models.StatisticsEvent;

public enum StatisticsEventToEntityConverter implements Converter<StatisticsEvent, Statistics> {

    STATISTICS_EVENT_TO_ENTITY_CONVERTER;

    @Override
    public Statistics convert(StatisticsEvent event) {
        Statistics entity =  new Statistics();
        entity.setType(event.getType());
        entity.setOwner(event.getOwner());
        entity.setTime(event.getTime());

        return entity;
    }
}
