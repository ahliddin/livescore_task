package utils.converters;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public interface Converter<FROM, TO> {
    TO convert(FROM from);

    default List<TO> convert(Collection<FROM> source) {
        return source.stream()
                .map(this::convert)
                .collect(Collectors.toList());
    }
}
