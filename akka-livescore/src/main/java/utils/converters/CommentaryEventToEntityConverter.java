package utils.converters;

import entities.Commentary;
import models.CommentaryEvent;

public enum CommentaryEventToEntityConverter implements Converter<CommentaryEvent, Commentary> {

    COMMENTARY_EVENT_TO_ENTITY_CONVERTER;

    @Override
    public Commentary convert(CommentaryEvent event) {
        Commentary entity = new Commentary();
        entity.setText(event.getText());
        entity.setTime(event.getTime());
        return entity;
    }
}
