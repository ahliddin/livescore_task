package utils.converters;

import entities.Statistics;
import models.StatisticsEvent;

public enum StatisticsEntityToEventConverter implements Converter<Statistics, StatisticsEvent> {

    STATISTICS_ENTITY_TO_EVENT_CONVERTER;

    @Override
    public StatisticsEvent convert(Statistics entity) {
        return new StatisticsEvent(entity.getType(), entity.getTime(), entity.getOwner());
    }
}
