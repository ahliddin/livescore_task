package utils;

import entities.enums.StatisticsType;
import models.CommentaryEvent;
import models.StatisticsEvent;

import java.time.LocalDateTime;

import static org.apache.commons.lang3.RandomUtils.nextInt;

public class RandomSoccerEventProvider implements SoccerEventProvider {
    private final static String[] PLAYERS = {"Mbappe", "Ronaldo", "Messi", "Ibragimovic"};
    private final static String[] COMMENTARIES = {"SHOT!", "Mbappe is running behind Marcelo", "A brilliant, brilliant move!", "Missed opportunity", "It's getting boring"};

    @Override
    public StatisticsEvent getStatistics(int ignored) {
        return new StatisticsEvent(StatisticsType.values()[nextInt(0, StatisticsType.values().length)], LocalDateTime.now(), PLAYERS[nextInt(0, PLAYERS.length)]);
    }

    @Override
    public CommentaryEvent getCommentary(int ignored) {
        return new CommentaryEvent(LocalDateTime.now(), COMMENTARIES[nextInt(0, COMMENTARIES.length)]);
    }


}
