package persistence;

import entities.SoccerMatch;
import models.CommentaryEvent;
import models.SoccerMatchEvent;
import models.StatisticsEvent;

import java.util.List;
import java.util.stream.Collectors;

import static utils.converters.CommentaryEntityToEventConverter.COMMENTARY_ENTITY_TO_EVENT_CONVERTER;
import static utils.converters.StatisticsEntityToEventConverter.STATISTICS_ENTITY_TO_EVENT_CONVERTER;

/**
 * Wrapper service over repository to provide converting logic from entity to event model
 */
public class SoccerEventFetchingService {
    private final PersistenceRepository repository;

    public SoccerEventFetchingService(PersistenceRepository repository) {
        this.repository = repository;
    }

    public SoccerMatchEvent fetchSoccerMatchEvent(int matchId) {
        List<CommentaryEvent> commentaries = COMMENTARY_ENTITY_TO_EVENT_CONVERTER.convert(repository.findCommentaries(matchId));
        List<StatisticsEvent> statistics = STATISTICS_ENTITY_TO_EVENT_CONVERTER.convert(repository.findStatistics(matchId));

        return new SoccerMatchEvent(matchId, commentaries, statistics);
    }

    public List<SoccerMatchEvent> fetchAllSoccerMatchEvents() {
        return repository.findAllSoccerMatches().stream()
                .map(SoccerMatch::getMatchId)
                .map(this::fetchSoccerMatchEvent)
                .collect(Collectors.toList());
    }
}
