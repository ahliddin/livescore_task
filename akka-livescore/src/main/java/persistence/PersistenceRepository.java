package persistence;

import entities.Commentary;
import entities.SoccerMatch;
import entities.Statistics;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;
import java.util.Optional;


public class PersistenceRepository {
    private final EntityManagerFactory entityManagerFactory;
    private final EntityManager entityManager;

    public PersistenceRepository() {
        this.entityManagerFactory = Persistence.createEntityManagerFactory("livescoreDB");
        this.entityManager = entityManagerFactory.createEntityManager();
    }

    public void close() {
        entityManager.close();
        entityManagerFactory.close();
    }

    public void persist(Object entity) {
        entityManager.getTransaction().begin();
        entityManager.persist(entity);
        entityManager.getTransaction().commit();
    }

    public List<Commentary> findCommentaries(int matchId) {
        return entityManager.createQuery("SELECT c from Commentary c where c.soccerMatch.matchId = :matchId", Commentary.class)
                .setParameter("matchId", matchId)
                .getResultList();

    }

    public List<Statistics> findStatistics(int matchId) {
        return entityManager.createQuery("SELECT s from Statistics s where s.soccerMatch.matchId = :matchId", Statistics.class)
                .setParameter("matchId", matchId)
                .getResultList();

    }

    public List<SoccerMatch> findAllSoccerMatches() {
        return entityManager
                .createQuery("SELECT s from SoccerMatch s", SoccerMatch.class)
                .getResultList();
    }

    public Optional<SoccerMatch> findSoccerMatch(int matchId) {
        return entityManager
                .createQuery("SELECT s from SoccerMatch s where s.matchId = :matchId", SoccerMatch.class)
                .setParameter("matchId", matchId)
                .getResultStream()
                .findFirst();
    }

}
