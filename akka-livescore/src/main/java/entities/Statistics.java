package entities;


import com.fasterxml.jackson.annotation.JsonIgnore;
import entities.enums.StatisticsType;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "STATISTICS")
public class Statistics {

    @Id
    @Column(name = "ID", nullable = false)
    private String id = UUID.randomUUID().toString();

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinColumn(name = "MATCH_ID", nullable = false)
    private SoccerMatch soccerMatch;

    @Enumerated(EnumType.STRING)
    @Column(name = "TYPE", nullable = false)
    private StatisticsType type;

    @Column(name = "OWNER", nullable = false)
    private String owner;

    @Column(name = "TIME", nullable = false)
    private LocalDateTime time;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SoccerMatch getSoccerMatch() {
        return soccerMatch;
    }

    public void setSoccerMatch(SoccerMatch soccerMatch) {
        this.soccerMatch = soccerMatch;
    }

    public StatisticsType getType() {
        return type;
    }

    public void setType(StatisticsType type) {
        this.type = type;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Statistics{" +
                "id='" + id + '\'' +
                ", type=" + type +
                ", owner='" + owner + '\'' +
                ", time=" + time +
                '}';
    }
}
