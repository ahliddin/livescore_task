package entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table( name = "SOCCER_MATCH")
public class SoccerMatch {

    @Id
    @Column(name = "MATCH_ID", nullable = false)
    private Integer matchId;

    public SoccerMatch() { // for hibernate
    }

    public SoccerMatch(Integer matchId) {
        this.matchId = matchId;
    }

    public Integer getMatchId() {
        return matchId;
    }

    public void setMatchId(Integer matchId) {
        this.matchId = matchId;
    }

    @Override
    public String toString() {
        return "SoccerMatch{" +
                "matchId=" + matchId +
                '}';
    }
}
