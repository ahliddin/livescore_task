package entities.enums;

public enum StatisticsType {
    YELLOW_CARD,
    RED_CARD,
    GOAL,
    NOTHING
}
