package entities;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "COMMENTARY")
public class Commentary {

    @Id
    private String id = UUID.randomUUID().toString();

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinColumn(name = "MATCH_ID", nullable = false)
    private SoccerMatch soccerMatch;

    @Column(name = "TEXT", nullable = false)
    private String text;

    @Column(name = "TIME", nullable = false)
    private LocalDateTime time;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SoccerMatch getSoccerMatch() {
        return soccerMatch;
    }

    public void setSoccerMatch(SoccerMatch soccerMatch) {
        this.soccerMatch = soccerMatch;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Commentary{" +
                "id='" + id + '\'' +
                ", text='" + text + '\'' +
                ", time=" + time +
                '}';
    }
}
