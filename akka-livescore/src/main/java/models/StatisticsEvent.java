package models;

import entities.enums.StatisticsType;

import java.time.LocalDateTime;

public final class StatisticsEvent {
    private final StatisticsType type;
    private final LocalDateTime time;
    private final String owner;

    public StatisticsEvent(StatisticsType type, LocalDateTime time, String owner) {
        this.type = type;
        this.time = time;
        this.owner = owner;
    }

    public StatisticsType getType() {
        return type;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public String getOwner() {
        return owner;
    }


    @Override
    public String toString() {
        return "StatisticsEvent{" +
                "type=" + type +
                ", time=" + time +
                ", owner='" + owner + '\'' +
                '}';
    }
}
