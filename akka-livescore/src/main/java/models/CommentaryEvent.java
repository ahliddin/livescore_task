package models;

import java.time.LocalDateTime;

public final class CommentaryEvent {
    private final LocalDateTime time;
    private final String text;

    public CommentaryEvent(LocalDateTime time, String text) {
        this.time = time;
        this.text = text;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return "CommentaryEvent{" +
                "time=" + time +
                ", text='" + text + '\'' +
                '}';
    }
}
