package models;

import java.util.List;

/**
 * Note: this class is not immutable due to collections being received/returned.
 * Consider creating copies of the collections when received and returned
 */
public final class SoccerMatchEvent {
    private final int matchId;
    private final List<CommentaryEvent> commentaries;
    private final List<StatisticsEvent> statistics;

    public SoccerMatchEvent(int matchId, List<CommentaryEvent> commentaries, List<StatisticsEvent> statistics) {
        this.matchId = matchId;
        this.commentaries = commentaries;
        this.statistics = statistics;
    }

    public int getMatchId() {
        return matchId;
    }

    public List<CommentaryEvent> getCommentaries() {
        return commentaries;
    }

    public List<StatisticsEvent> getStatistics() {
        return statistics;
    }
}
