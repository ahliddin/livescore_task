package com.programmer9.validation;

import akka.actor.AbstractActor;
import akka.actor.AbstractLoggingActor;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic;
import java.util.Set;

@SupportedAnnotationTypes("com.programmer9.validation.Actor")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class ActorProcessor extends AbstractProcessor {
    private Messager messager;

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {

        Set<? extends Element> actorAnnotated = roundEnv.getElementsAnnotatedWith(Actor.class);
        for (Element e : actorAnnotated) {
            if (e.getKind().isClass()) {
                validate((TypeElement) e);
            }
        }

        return false;
    }

    private void validate(TypeElement typeElement) {
        TypeMirror superClass = typeElement.getSuperclass();
        if (!superClass.toString().contains(AbstractActor.class.getName()) && !superClass.toString().contains(AbstractLoggingActor.class.getName())) {
            messager.printMessage(Diagnostic.Kind.ERROR, "Actor annotated class is not extending AbstractActor or AbstractLoggingActor", typeElement);
        }
    }

    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        this.messager = processingEnv.getMessager();
    }
}
